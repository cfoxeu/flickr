const path = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const extractSass = new ExtractTextPlugin({
    filename: 'public/index.css'
})

let commonLoaders = [
    {
        test: /\.json$/,
        loader: 'json-loader'
    },
]

module.exports = [
    {
        entry: './src/server.js',
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'server.js',
            libraryTarget: 'commonjs2',
            publicPath: '/'
        },
        target: 'node',
        node: {
            console: false,
            global: false,
            process: false,
            Buffer: false,
            __filename: false,
            __dirname: false
        },
        externals: nodeExternals(),
        module: {
            loaders: [
                {
                    test: /\.(js|jsx)$/,
                    use: 'babel-loader'
                },
                {
                    test: /\.(s?)css$/,
                    exclude: /node_modules/,
                    use: extractSass.extract({
                        fallback: 'style-loader',
                        use: ['css-loader', 'sass-loader'],
                    }),
                },
            ].concat(commonLoaders)
        },
        plugins: [
            extractSass
        ],
        resolve: {
            extensions: ['.js', '.jsx']
        },
    },
    {
        entry: './src/app/App.js',
        output: {
            path: path.resolve(__dirname, './dist/public'),
            publicPath: '/',
            filename: 'bundle.js'
        },
        module: {
            loaders: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: 'babel-loader'
                },
                {
                    test: /\.(s?)css$/,
                    exclude: /node_modules/,
                    use: extractSass.extract({
                        fallback: 'style-loader',
                        use: ['css-loader', 'sass-loader'],
                    }),
                },
            ].concat(commonLoaders)
        },
        plugins: [

            extractSass
        ],
        resolve: {
            extensions: ['.js', '.jsx']
        }
    }
];
