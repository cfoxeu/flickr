import React, { Component } from 'react'
import Icon from 'react-fontawesome'

import './main.scss'

import Header from './components/Header'
import Footer from './components/Footer'
import Post from './components/Post'


export default class Homepage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            theme: false // Default to false = light, true = dark
        }

        this.switchTheme = this.switchTheme.bind(this)
        this.filterResults = this.filterResults.bind(this)
    }
    switchTheme() { // Changes between light and dark themes
        this.setState({
            theme: !this.state.theme
        })
    }
    filterResults() { // Filter results by tags
    }
    render() {
        const { theme } = this.state
        const { data } = this.props.data && this.props
        return (
            <div className="page-wrapper">
                <div className={ `themer ${theme ? 'dark' : 'light'}` }>
                    <Header />
                    {/* <button className="btn theme-switcher" onClick={this.switchTheme}>
                        { theme ?<Icon name='sun-o' /> :<Icon name='moon-o' /> }
                    </button> */}
                    <main>
                        {/* <input type="text" id="myInput" onKeyUp="filterResults()" placeholder="Search by tags.." /> */}
                        <section className="item-list">
                            { data.items.map(
                                (item, index) => {
                                    return (
                                        <Post {...item} key={index} />
                                    )
                                })
                            }
                        </section>
                    </main>
                    <Footer />
                </div>
            </div>
        )
    }
}
