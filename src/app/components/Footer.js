import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <footer>
                <h5>Flickr app - Chris Fox - 2018</h5>
            </footer>
        )
    }
}
