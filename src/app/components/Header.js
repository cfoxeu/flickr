import React, { Component } from 'react';

export default class Header extends Component {
    render() {
        return (
            <header>
                <h1>Flickr SPA</h1>
            </header>
        )
    }
}
