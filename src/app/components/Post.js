import React, { Component } from 'react';

export default class Post extends Component {
    render() {
        const {
            link,
            media,
            title,
            author,
            author_id,
            published,
            tags,
        } = this.props
        // Insert tags into an array
        const tagsArray = tags.split(' ')
        // Create author profile link // Defaults to /photos
        const authorProfileLink = `https://www.flickr.com/${author_id}`
        // Format published date
        const tidyString = (string, type, update = '') => {
            if (type === 'date') {
                // Create datetime string
                const dta = new Date(string).toString()
                // Remove extra fluff
                .replace(/( GMT\+0000 \(GMT\))/g, update)
                .substring(4)
                .split(' ')
                return `Published ${dta[1]} ${dta[0]} ${dta[2]} at ${dta[3]}`
            }
            if (type === 'author') {
                return string.replace(/(nobody@flickr.com |\("|"\))/g, update)
            }
        }
        return (
            <article>
                <figure>
                    <a href={authorProfileLink}>
                        <img src={media.m} alt={ title }/>
                    </a>
                </figure>
                <figcaption>
                    <h3 className="title">{title}</h3>
                    <a className="author" href={authorProfileLink}>{tidyString(author, 'author')}</a>
                    <p className="published">{tidyString(published, 'date')}</p>
                    <a className="link" href={link}>View on Flickr</a>
                </figcaption>
            </article>
        )
    }
}
