const path = require('path')
import express from 'express'
import React from 'react'
import { renderToString } from 'react-dom/server'

import App from './app/Homepage'
import template from './template'
import 'isomorphic-fetch'

const server = express()

server.use(express.static(path.join(__dirname + '/public')))

server.get('/', (req, res) => {
    let initialState = null
    let appString = null
    const baseUrl = 'https://api.flickr.com/services/feeds/photos_public.gne'
    const urlParams = [
        'safe_search=1',
        'format=json',
        'nojsoncallback=1',
    ]
    // Join baseUrl with ?urlParams separated by &
    const url = `${baseUrl + (urlParams ? '?' + urlParams.join('&') : '')}`
    fetch(url)
    .then(res => res.json())
    .then(json => {
        initialState = { data: json }
        appString = renderToString(<App {...initialState} />)
        res.send(template({
            title: 'Flickr SPA',
            body: appString,
            initialState: JSON.stringify(initialState)
        }))
    })
    .catch(error => {
        console.log("Something went wrong. ", error)
    })
})

server.listen(3000)
console.log('listening')
