# Serverside React flickr app

## Get started

- `git clone https://cfoxeu@bitbucket.org/cfoxeu/flickr.git` to get the app

- Navigate into the project directory - `cd flickr`

- `yarn` to install dependencies
(If you don't have yarn grab it now - https://yarnpkg.com/lang/en/docs/install/)

- `yarn build` will build your static files (optional)

- `yarn start` will run the app

Find the app in your browser @`localhost:3000`


## App structure

```
|---| /src Contains server and html template
|---|---| /app Contains all app files
|---|---|---| /components Components refer to portions of a page, often reusable
|---|---|---| /styles Contains preprocessed stylesheets (.scss)
|---|---|--- App.js Renders app into root element
|---|---|--- Homepage.js Landing page and main spa layout
|---|---|--- main.scss Contains imports for all other .scss files
|---|--- server.js Serves app and handles requests
|---|--- template.js HTML template containing imports for custom scripts and stylesheets
```

## Extra notes

- `/src/app/styles/_NAME.scss` imports maintain the `_[name].[ext]` naming convention.
- Imports should be modular, that is it refers to a single instance or is shared.
